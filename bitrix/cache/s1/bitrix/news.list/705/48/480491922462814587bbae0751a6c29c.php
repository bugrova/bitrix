<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001579616315';
$dateexpire = '001615616315';
$ser_content = 'a:2:{s:7:"CONTENT";s:52696:"<div class="news-list">
		<p class="news-item" id="bx_3218110189_533">
													<a href="/testing/?ELEMENT_ID=533"><b>Выпуск#29: ITренировка — актуальные вопросы и задачи от ведущих компаний</b></a><br />
														<small>
			Название:&nbsp;Выпуск#29: ITренировка — актуальные вопросы и задачи от ведущих компаний			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;14:37:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							Привет! Надеемся, вы на УРА отдохнули в новогодние праздники. <br>
<br>
<img src="/upload/ebx89cutdtisffssraq3kmny39s.png" alt="image"><br>
<br>
А мы вот времени зря не теряли и подготовили новую подборку вопросов и задач. Сегодня — задачки с собеседований в VMWare. VMware — американская компания, крупнейший разработчик программного обеспечения для виртуализации. Штаб-квартира расположена в Пало-Альто, Калифорния. Ну что, проверим ваши шансы пройти у них собеседования? <br>
<br>
Кстати, ответы на предыдущие задачки уже <a href="https://habr.com/ru/company/spice/blog/479320/">опубликованы</a>! Сверяйтесь с ними. <a href="https://habr.com/ru/post/484276/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484276#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484276%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484276&amp;event3=%D0%92%D1%8B%D0%BF%D1%83%D1%81%D0%BA%2329%3A+IT%D1%80%D0%B5%D0%BD%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0+%E2%80%94+%D0%B0%D0%BA%D1%82%D1%83%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5+%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D1%8B+%D0%B8+%D0%B7%D0%B0%D0%B4%D0%B0%D1%87%D0%B8+%D0%BE%D1%82+%D0%B2%D0%B5%D0%B4%D1%83%D1%89%D0%B8%D1%85+%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B9&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484276%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484276">https://habr.com/ru/post/484276/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484276</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_532">
													<a href="/testing/?ELEMENT_ID=532"><b>Пилотный проект по обработке высокоплотных сейсмических данных с использованием сервиса MCS</b></a><br />
														<small>
			Название:&nbsp;Пилотный проект по обработке высокоплотных сейсмических данных с использованием сервиса MCS			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;14:58:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							<img src="/upload/y_hp7p1goiagpvg4wh8q1o6s16u.jpeg"><br>
<br>
Компания ООО НПЦ «Геостра» с помощью сервиса <a href="https://mcs.mail.ru/">MCS</a> провела камеральную обработку сейсмической информации — 40 Тб высокоплотной съёмки МОГТ-3D. О реализации, нюансах и результатах проекта будет рассказано в данной статье.<br> <a href="https://habr.com/ru/post/484662/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484662#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484662%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484662&amp;event3=%D0%9F%D0%B8%D0%BB%D0%BE%D1%82%D0%BD%D1%8B%D0%B9+%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82+%D0%BF%D0%BE+%D0%BE%D0%B1%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B5+%D0%B2%D1%8B%D1%81%D0%BE%D0%BA%D0%BE%D0%BF%D0%BB%D0%BE%D1%82%D0%BD%D1%8B%D1%85+%D1%81%D0%B5%D0%B9%D1%81%D0%BC%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D1%85+%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85+%D1%81+%D0%B8%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5%D0%BC+%D1%81%D0%B5%D1%80%D0%B2%D0%B8%D1%81%D0%B0+MCS&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484662%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484662">https://habr.com/ru/post/484662/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484662</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_531">
													<a href="/testing/?ELEMENT_ID=531"><b>[Из песочницы] Возвращение GOTO</b></a><br />
														<small>
			Название:&nbsp;[Из песочницы] Возвращение GOTO			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;15:10:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							<p>Сейчас все понимают, что использовать оператор GOTO это не просто плохая, а ужасная практика. Дебаты по поводу его использования закончились в 80-х годах XX века и его исключили из большинства современных языков программирования. Но, как и положено настоящему злу, он сумел замаскироваться и воскреснуть в XXI веке под видом исключений. </p> <a href="https://habr.com/ru/post/484840/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484840#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484840%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484840&amp;event3=%5B%D0%98%D0%B7+%D0%BF%D0%B5%D1%81%D0%BE%D1%87%D0%BD%D0%B8%D1%86%D1%8B%5D+%D0%92%D0%BE%D0%B7%D0%B2%D1%80%D0%B0%D1%89%D0%B5%D0%BD%D0%B8%D0%B5+GOTO&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484840%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484840">https://habr.com/ru/post/484840/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484840</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_530">
													<a href="/testing/?ELEMENT_ID=530"><b>[Перевод] Запускаем игру на C# в MS-DOS</b></a><br />
														<small>
			Название:&nbsp;[Перевод] Запускаем игру на C# в MS-DOS			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;16:24:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							Меня всегда раздражало, что я не могу запустить 64-битную игру на C# под MS-DOS. Сегодня я это исправил.<br>
<br>
<img src="https://habrastorage.org/getpro/habr/post_images/d4a/67c/7c9/d4a67c7c9bdca5399adfeca43df14531.gif"><br> <a href="https://habr.com/ru/post/484854/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484854#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484854%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484854&amp;event3=%5B%D0%9F%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B4%5D+%D0%97%D0%B0%D0%BF%D1%83%D1%81%D0%BA%D0%B0%D0%B5%D0%BC+%D0%B8%D0%B3%D1%80%D1%83+%D0%BD%D0%B0+C%23+%D0%B2+MS-DOS&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484854%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484854">https://habr.com/ru/post/484854/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484854</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_529">
													<a href="/testing/?ELEMENT_ID=529"><b>[Перевод] Название имплементации и название результата</b></a><br />
														<small>
			Название:&nbsp;[Перевод] Название имплементации и название результата			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;16:53:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							<p><img src="/upload/sw9afbxo38dofpspvffb0c_jrq4.jpeg"></p><br>
<p>Я хотел написать этот пост ещё в июле, но никак не мог, <em>о ирония</em>, решить, как его назвать. Удачные термины пришли мне в голову только после <a href="https://www.youtube.com/watch?v=MBRoCdtZOYg" rel="nofollow">доклада Кейт Грегори на CppCon</a>, и теперь я наконец могу рассказать вам, как не надо называть функции.<br>
Бывают, конечно, названия, которые вообще не несут информации, типа <code>int f(int x)</code>. Ими пользоваться тоже не надо, но речь не о них. Порой бывает, что вроде бы и информации в названии полно, но пользы от неё абсолютно никакой.</p> <a href="https://habr.com/ru/post/484860/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484860#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484860%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484860&amp;event3=%5B%D0%9F%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B4%5D+%D0%9D%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5+%D0%B8%D0%BC%D0%BF%D0%BB%D0%B5%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D0%B8+%D0%B8+%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5+%D1%80%D0%B5%D0%B7%D1%83%D0%BB%D1%8C%D1%82%D0%B0%D1%82%D0%B0&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484860%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484860">https://habr.com/ru/post/484860/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484860</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_528">
													<a href="/testing/?ELEMENT_ID=528"><b>[Из песочницы] Миграция с AngularJS на Angular7 через гибридное приложение</b></a><br />
														<small>
			Название:&nbsp;[Из песочницы] Миграция с AngularJS на Angular7 через гибридное приложение			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;16:55:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							<h2>Переход с AngularJS на Angular7 через гибридное приложение</h2><br/>
<br/>
Задача не самая простая, но выполнимая.<br/>
<br/>
С ней я столкнулась при переходе в новую компанию. <a href="https://habr.com/ru/post/484862/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484862#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484862%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484862&amp;event3=%5B%D0%98%D0%B7+%D0%BF%D0%B5%D1%81%D0%BE%D1%87%D0%BD%D0%B8%D1%86%D1%8B%5D+%D0%9C%D0%B8%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D1%8F+%D1%81+AngularJS+%D0%BD%D0%B0+Angular7+%D1%87%D0%B5%D1%80%D0%B5%D0%B7+%D0%B3%D0%B8%D0%B1%D1%80%D0%B8%D0%B4%D0%BD%D0%BE%D0%B5+%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484862%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484862">https://habr.com/ru/post/484862/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484862</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_527">
													<a href="/testing/?ELEMENT_ID=527"><b>[recovery mode] Как измерить улучшения в команде? Часть 2</b></a><br />
														<small>
			Название:&nbsp;[recovery mode] Как измерить улучшения в команде? Часть 2			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;16:57:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							<p>Эта статья – продолжение повествования, начатого <a href="https://habr.com/ru/post/483754/">здесь</a>.</p><br>
<p>Я понимал, что сам Пентланд – в силу своего статуса – вряд ли что-то делает сам, и выступает скорее в роли вдохновителя, научного руководителя, публичного представителя. Всё полезное, что он мог бы мне дать, можно прочитать в его открытых публикациях. Меня же интересовали люди, которые весь этот проект со сбором и обработкой социометрических данных делают своими руками.</p> <a href="https://habr.com/ru/post/484868/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484868#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484868%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484868&amp;event3=%5Brecovery+mode%5D+%D0%9A%D0%B0%D0%BA+%D0%B8%D0%B7%D0%BC%D0%B5%D1%80%D0%B8%D1%82%D1%8C+%D1%83%D0%BB%D1%83%D1%87%D1%88%D0%B5%D0%BD%D0%B8%D1%8F+%D0%B2+%D0%BA%D0%BE%D0%BC%D0%B0%D0%BD%D0%B4%D0%B5%3F+%D0%A7%D0%B0%D1%81%D1%82%D1%8C+2&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484868%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484868">https://habr.com/ru/post/484868/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484868</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_526">
													<a href="/testing/?ELEMENT_ID=526"><b>Часть 6: Портирование MemTest86+ на RISC-V</b></a><br />
														<small>
			Название:&nbsp;Часть 6: Портирование MemTest86+ на RISC-V			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;17:06:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							<img src="/upload/nefbhar5ihkfmvccfwp0jqrm78u.png"><br>
<p>Наверное, мало какому айтишнику нужно объяснять, что такое Memtest86+ — пожалуй, он уже стал более-менее стандартом в тестировании оперативной памяти на ПК. Когда в одной из <a href="https://habr.com/ru/post/459470/">предыдущих частей</a> я наткнулся на битую планку памяти, пришедшую в комплекте с платой, он (вместе с поддерживающим DDR2 нетбуком) казался очевидным решением. Другой вопрос, что там в принципе нестабильная работа системы была видна невооружённым глазом. В более хитрых случаях, слышал, что кроме банального «простукивания» ячеек памяти до бесконечности, этот инструмент использует некоторые специальные паттерны данных, на которых ошибки в работе DDR выявляются с большей вероятностью. В общем чудесная вещь, жаль, что даже в названии указано: 86 — «Только для x86-совместимых систем». Или нет?</p><br>
<p>Под катом вы увидите мои попытки портировать MemTest86+ v5.1 на RISC-V и промежуточный итог. <em>Спойлер: оно шевелится!</em></p> <a href="https://habr.com/ru/post/484026/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484026#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484026%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484026&amp;event3=%D0%A7%D0%B0%D1%81%D1%82%D1%8C+6%3A+%D0%9F%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5+MemTest86%2B+%D0%BD%D0%B0+RISC-V&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484026%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484026">https://habr.com/ru/post/484026/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484026</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_525">
													<a href="/testing/?ELEMENT_ID=525"><b>[Из песочницы] «Ваш айтишник сломался, несите нового»: как построить карьеру в IT и не сойти с ума</b></a><br />
														<small>
			Название:&nbsp;[Из песочницы] «Ваш айтишник сломался, несите нового»: как построить карьеру в IT и не сойти с ума			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;17:10:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							В прошлом году компания Ivanti – один из ключевых мировых разработчиков решений для управления ИТ-инфраструктурой – <a href="https://www.ivanti.com/webinars/2019/the-rigors-of-the-job-it-and-your-mental-health">опубликовала</a> запись вебинара об актуальных проблемах психического здоровья ИТ-специалистов. Старший вице-президент Ivanti Кевин Смит считает, что основной проблемой, стоявшей перед ИТ-специалистами компании в 2019 году, стало эмоциональное выгорание. Поле для такого анализа рынка у Ivanti достаточно большое – обладая штатом в 1700 человек и офисами в 23 странах, они предоставляют ИТ-решения клиентам по всему миру: только в России услугами Ivanti пользуются Сбербанк-КИБ, Счетная Палата, Башнефть, Вертолеты России, и еще более десятка правительственных и промышленных корпораций.<br/>
<br/>
По словам Смита, по мере того как нашу жизнь заполняют цифровые технологии, граница между «рабочим» и «личным» размывается абсолютно у всех. Однако ИТ-специалисты в силу специфики работы испытывают эту проблему максимально остро. «Основная задача, которая стояла перед ИТ-отраслью в последние 30 лет – повысить производительность труда, снизив при этом временные затраты», – объясняет Смит. И эта задача, по его словам, породила новые, гораздо более высокие требования к концентрации и самодисциплине ИТ-специалистов. <br/>
 <a href="https://habr.com/ru/post/484874/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484874#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484874%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484874&amp;event3=%5B%D0%98%D0%B7+%D0%BF%D0%B5%D1%81%D0%BE%D1%87%D0%BD%D0%B8%D1%86%D1%8B%5D+%C2%AB%D0%92%D0%B0%D1%88+%D0%B0%D0%B9%D1%82%D0%B8%D1%88%D0%BD%D0%B8%D0%BA+%D1%81%D0%BB%D0%BE%D0%BC%D0%B0%D0%BB%D1%81%D1%8F%2C+%D0%BD%D0%B5%D1%81%D0%B8%D1%82%D0%B5+%D0%BD%D0%BE%D0%B2%D0%BE%D0%B3%D0%BE%C2%BB%3A+%D0%BA%D0%B0%D0%BA+%D0%BF%D0%BE%D1%81%D1%82%D1%80%D0%BE%D0%B8%D1%82%D1%8C+%D0%BA%D0%B0%D1%80%D1%8C%D0%B5%D1%80%D1%83+%D0%B2+IT+%D0%B8+%D0%BD%D0%B5+%D1%81%D0%BE%D0%B9%D1%82%D0%B8+%D1%81+%D1%83%D0%BC%D0%B0&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484874%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484874">https://habr.com/ru/post/484874/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484874</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_524">
													<a href="/testing/?ELEMENT_ID=524"><b>Починить, хакнуть, раскопать. Решаем онлайн-квест Droid Mission</b></a><br />
														<small>
			Название:&nbsp;Починить, хакнуть, раскопать. Решаем онлайн-квест Droid Mission			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;17:13:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							<img src="/upload/r_ysqsq8esvuaug1phjeithvpji.png"><br>
<br>
В прошлом году мы провели онлайн-квест для мобильных разработчиков — Droid Mission. В течение месяца участники должны были решить как можно больше задач в трёх направлениях: fix it! (поиск ошибок и исследование кода), hack it! (реверс-инжиниринг) и dig it! (изучение особенностей Android). Всего в квесте было 23 задачи — они очень похожи на те, с которыми сталкиваются специалисты по Android в реальной работе. В посте мы покажем все условия и правильные решения.<br> <a href="https://habr.com/ru/post/484876/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484876#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484876%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484876&amp;event3=%D0%9F%D0%BE%D1%87%D0%B8%D0%BD%D0%B8%D1%82%D1%8C%2C+%D1%85%D0%B0%D0%BA%D0%BD%D1%83%D1%82%D1%8C%2C+%D1%80%D0%B0%D1%81%D0%BA%D0%BE%D0%BF%D0%B0%D1%82%D1%8C.+%D0%A0%D0%B5%D1%88%D0%B0%D0%B5%D0%BC+%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD-%D0%BA%D0%B2%D0%B5%D1%81%D1%82+Droid+Mission&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484876%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484876">https://habr.com/ru/post/484876/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484876</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_523">
													<a href="/testing/?ELEMENT_ID=523"><b>[Перевод] Прощай, чистый код</b></a><br />
														<small>
			Название:&nbsp;[Перевод] Прощай, чистый код			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;12:30:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							<a href="https://habr.com/ru/company/ruvds/blog/484610/"><img src="/upload/dsrfkmov-s5b1wgeuy1obsbsj7k.jpeg"></a><br>
<br>
Был поздний вечер.<br>
<br>
Мой коллега только что записал в репозиторий код, над которым работал целую неделю. Мы делали тогда графический редактор, а в свежем коде были реализованы возможности по изменению фигур, находящихся в рабочем поле. Фигуры, например — прямоугольники и овалы, можно было модифицировать, перемещая небольшие маркеры, расположенные на их краях.<br>
<br>
Код работал.<br>
<br>
Но в нём было много повторяющихся однотипных конструкций. Каждая фигура (вроде того же прямоугольника или овала) обладала различным набором маркеров. Перемещение этих маркеров в разных направлениях по-разному влияло на позицию и размер фигуры. А если пользователь, двигая маркеры, удерживал нажатой клавишу Shift, нам, кроме того, надо было сохранять пропорции фигуры при изменении её размера. В общем — в коде было много вычислений.<br> <a href="https://habr.com/ru/post/484610/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484610#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484610%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484610&amp;event3=%5B%D0%9F%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B4%5D+%D0%9F%D1%80%D0%BE%D1%89%D0%B0%D0%B9%2C+%D1%87%D0%B8%D1%81%D1%82%D1%8B%D0%B9+%D0%BA%D0%BE%D0%B4&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484610%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484610">https://habr.com/ru/post/484610/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484610</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_522">
													<a href="/testing/?ELEMENT_ID=522"><b>DIY Корутины. Часть 1. Ленивые генераторы</b></a><br />
														<small>
			Название:&nbsp;DIY Корутины. Часть 1. Ленивые генераторы			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;12:48:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							<p>В мире JVM про корутины знают в большей степени благодаря языку Kotlin и <a href="https://habr.com/ru/company/jugru/blog/422519/">Project Loom</a>. Хорошего описания принципа работы котлиновских корутин я не видел, а код библиотеки kotlin-coroutines совершенно не понятен неподготовленному человеку. По моему опыту, большинство людей знает о корутинах только то, что это &quot;облегченные потоки&quot;, и что в котлине они работают через умную генерацию байткода. Таким был и я до недавнего времени. И мне пришла в голову идея, что раз корутины могут быть реализованы в байткоде, то почему бы не реализовать их в java. Из этой идеи, впоследствии, появилась небольшая и достаточно простая библиотека, в устройстве которой, я надеюсь, может разобраться практически любой разработчик. Подробности под катом. </p><br>
<p><img src="/upload/2_7fs4po74_uoaqwmucjmnyjcuw.jpeg"></p> <a href="https://habr.com/ru/post/480888/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=480888#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F480888%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D480888&amp;event3=DIY+%D0%9A%D0%BE%D1%80%D1%83%D1%82%D0%B8%D0%BD%D1%8B.+%D0%A7%D0%B0%D1%81%D1%82%D1%8C+1.+%D0%9B%D0%B5%D0%BD%D0%B8%D0%B2%D1%8B%D0%B5+%D0%B3%D0%B5%D0%BD%D0%B5%D1%80%D0%B0%D1%82%D0%BE%D1%80%D1%8B&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F480888%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D480888">https://habr.com/ru/post/480888/?utm_source=habrahabr&utm_medium=rss&utm_campaign=480888</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_521">
													<a href="/testing/?ELEMENT_ID=521"><b>[Из песочницы] Blazor: как не дать компоненту заболеть или два подхода для отделения кода от разметки</b></a><br />
														<small>
			Название:&nbsp;[Из песочницы] Blazor: как не дать компоненту заболеть или два подхода для отделения кода от разметки			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;12:55:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							<p>Думаю каждый разработчик, когда приходил на новый проект, думал о том, что было бы не плохо вернуться на машине времени назад и сказать отцам своего проекта, что паттерны нужно не только спрашивать на собеседовании, но и применять на реальном проекте, да я серьезно. </p><br>
<p>В частности, это наверное на паттерн, а очень хорошее правило, что разметку нужно отделять от кода. Это касается как и старых добрых Web Forms, Asp.Net MVC — кто-нибудь еще пишет разметку на Razor?), так и Angular популярного в суровом enterprise. </p><br>
<p>Если вы уже давно от безысходности пошли на поводу у фронт эндеров и переехали на новомодные Angular и React, то...</p> <a href="https://habr.com/ru/post/484822/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484822#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484822%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484822&amp;event3=%5B%D0%98%D0%B7+%D0%BF%D0%B5%D1%81%D0%BE%D1%87%D0%BD%D0%B8%D1%86%D1%8B%5D+Blazor%3A+%D0%BA%D0%B0%D0%BA+%D0%BD%D0%B5+%D0%B4%D0%B0%D1%82%D1%8C+%D0%BA%D0%BE%D0%BC%D0%BF%D0%BE%D0%BD%D0%B5%D0%BD%D1%82%D1%83+%D0%B7%D0%B0%D0%B1%D0%BE%D0%BB%D0%B5%D1%82%D1%8C+%D0%B8%D0%BB%D0%B8+%D0%B4%D0%B2%D0%B0+%D0%BF%D0%BE%D0%B4%D1%85%D0%BE%D0%B4%D0%B0+%D0%B4%D0%BB%D1%8F+%D0%BE%D1%82%D0%B4%D0%B5%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F+%D0%BA%D0%BE%D0%B4%D0%B0+%D0%BE%D1%82+%D1%80%D0%B0%D0%B7%D0%BC%D0%B5%D1%82%D0%BA%D0%B8&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484822%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484822">https://habr.com/ru/post/484822/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484822</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_520">
													<a href="/testing/?ELEMENT_ID=520"><b>Книга «C++. Практика многопоточного программирования»</b></a><br />
														<small>
			Название:&nbsp;Книга «C++. Практика многопоточного программирования»			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;12:56:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							<a href="https://habr.com/ru/company/piter/blog/484818/"><img src="/upload/crym3urkeecjcfe-nsvq0nrw59y.jpeg" align="left" alt="image"></a> Привет, Хаброжители! Язык С++ выбирают, когда надо создать по-настоящему молниеносные приложения. А качественная конкурентная обработка сделает их еще быстрее. Новые возможности С++17 позволяют использовать всю мощь многопоточного программирования, чтобы с легкостью решать задачи графической обработки, машинного обучения и др. Энтони Уильямс, эксперт конкурентной обработки, рассматривает примеры и описывает практические задачи, а также делится секретами, которые пригодятся всем, в том числе и самым опытным разработчикам. <br>
<br>
В книге • Полный обзор возможностей С++17. • Запуск и управление потоками. • Синхронизация конкурентных операций. • Разработка конкурентного кода. • Отладка многопоточных приложений. Книга подойдет для разработчиков среднего уровня, пользующихся C и C++. Опыт конкурентного программирования не требуется. <br> <a href="https://habr.com/ru/post/484818/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484818#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484818%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484818&amp;event3=%D0%9A%D0%BD%D0%B8%D0%B3%D0%B0+%C2%ABC%2B%2B.+%D0%9F%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D0%BA%D0%B0+%D0%BC%D0%BD%D0%BE%D0%B3%D0%BE%D0%BF%D0%BE%D1%82%D0%BE%D1%87%D0%BD%D0%BE%D0%B3%D0%BE+%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F%C2%BB&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484818%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484818">https://habr.com/ru/post/484818/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484818</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_519">
													<a href="/testing/?ELEMENT_ID=519"><b>Разница между cPanel и Plesk Obsidian</b></a><br />
														<small>
			Название:&nbsp;Разница между cPanel и Plesk Obsidian			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;13:00:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							Мы продолжаем рассказывать об удобных многофункциональных веб-консолях (панелях управления хостингом и сайтами), которые вы можете приобрести у нас вместе с <a href="https://ruvds.com/ru-rub">VPS</a> — об условиях приобретения читайте в конце статьи. Некоторые сравнения-обзоры консолей, которые есть в нашем арсенале, мы уже <a href="https://habr.com/ru/company/ruvds/blog/478830/">давали</a>, а также рассказывали о многих других панелях в <a href="https://habr.com/ru/company/ruvds/blog/468639/">общем обзоре</a>. В этой статье мы обсудим различия между <a href="https://habr.com/ru/company/ruvds/blog/476450/">cPanel</a> и <a href="https://habr.com/ru/company/ruvds/blog/473610/">Plesk Obsidian</a>, чтобы вы могли решить, какая из этих популярных консолей лучше может подойти под ваши задачи и опыт.<br>
<br>
<a href="https://habr.com/ru/company/ruvds/blog/484806/"><img src="/upload/f8dc5b3ed7b3a8a426eca519a03c7030.png"></a><br> <a href="https://habr.com/ru/post/484806/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484806#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484806%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484806&amp;event3=%D0%A0%D0%B0%D0%B7%D0%BD%D0%B8%D1%86%D0%B0+%D0%BC%D0%B5%D0%B6%D0%B4%D1%83+cPanel+%D0%B8+Plesk+Obsidian&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484806%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484806">https://habr.com/ru/post/484806/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484806</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_518">
													<a href="/testing/?ELEMENT_ID=518"><b>Использование перехватов операций для бэкапа файлов в macOS “на лету”</b></a><br />
														<small>
			Название:&nbsp;Использование перехватов операций для бэкапа файлов в macOS “на лету”			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;13:11:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							Привет, Хабр! Меня зовут Денис Копырин, и сегодня я хочу рассказать о том, как мы решали проблему бэкапа по требованию на macOS. На самом деле интересная задача, с которой я столкнулся в институте, выросла в итоге в большой проект по работе с файловой системой в macOS и стала частью системы Acronis Active Protection. Все подробности – под катом.<br>
<br>
<a href="https://habr.com/ru/company/acronis/blog/484816/"><img src="/upload/e8110c67b5fe5edb2a8259a8a68fe632.jpg" alt="image"></a><br> <a href="https://habr.com/ru/post/484816/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484816#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484816%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484816&amp;event3=%D0%98%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5+%D0%BF%D0%B5%D1%80%D0%B5%D1%85%D0%B2%D0%B0%D1%82%D0%BE%D0%B2+%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B9+%D0%B4%D0%BB%D1%8F+%D0%B1%D1%8D%D0%BA%D0%B0%D0%BF%D0%B0+%D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%B2+%D0%B2+macOS+%E2%80%9C%D0%BD%D0%B0+%D0%BB%D0%B5%D1%82%D1%83%E2%80%9D&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484816%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484816">https://habr.com/ru/post/484816/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484816</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_517">
													<a href="/testing/?ELEMENT_ID=517"><b>[Из песочницы] Как выстроить корпоративную стратегию обучения и развития</b></a><br />
														<small>
			Название:&nbsp;[Из песочницы] Как выстроить корпоративную стратегию обучения и развития			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;13:57:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							Всем привет! Я – Анна Хацько, HR-директор Omega-R. Моя работа связана с усилением стратегии обучения и развития в компании, и я хочу поделиться своим опытом и знаниями о том, как управлять профессиональным и карьерным ростом сотрудников таким образом, чтобы поддерживать другие ключевые приоритеты бизнеса.<br/>
<br/>
<img src="/upload/dnditnrnwc6bck_miegcqq70cgk.jpeg"/><br/>
 <a href="https://habr.com/ru/post/484834/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484834#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484834%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484834&amp;event3=%5B%D0%98%D0%B7+%D0%BF%D0%B5%D1%81%D0%BE%D1%87%D0%BD%D0%B8%D1%86%D1%8B%5D+%D0%9A%D0%B0%D0%BA+%D0%B2%D1%8B%D1%81%D1%82%D1%80%D0%BE%D0%B8%D1%82%D1%8C+%D0%BA%D0%BE%D1%80%D0%BF%D0%BE%D1%80%D0%B0%D1%82%D0%B8%D0%B2%D0%BD%D1%83%D1%8E+%D1%81%D1%82%D1%80%D0%B0%D1%82%D0%B5%D0%B3%D0%B8%D1%8E+%D0%BE%D0%B1%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D1%8F+%D0%B8+%D1%80%D0%B0%D0%B7%D0%B2%D0%B8%D1%82%D0%B8%D1%8F&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484834%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484834">https://habr.com/ru/post/484834/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484834</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_516">
													<a href="/testing/?ELEMENT_ID=516"><b>[Перевод] Мы приближаемся к пределу вычислительных мощностей – нам нужны новые программисты</b></a><br />
														<small>
			Название:&nbsp;[Перевод] Мы приближаемся к пределу вычислительных мощностей – нам нужны новые программисты			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;14:00:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							<h3>Все сильнее ускорявшиеся процессоры привели к появлению раздутого софта, но физические ограничения могут заставить нас вернуться к более скромному варианту кода, которым мы пользовались в прошлом</h3><br/>
<img src="https://i.guim.co.uk/img/media/cc3c43553c74bb41bdb53563800c588a5c5b4a69/0_74_7000_4200/master/7000.jpg?width=1920&amp;quality=85&amp;auto=format&amp;fit=max&amp;s=e44e1a69978ce97774b2a41773308a8b"/><br/>
<br/>
Давно, ещё в 1960-х, <a href="https://ru.wikipedia.org/wiki/Мур,_Гордон" rel="nofollow">Гордон Мур</a>, один из основателей компании Intel, заметил, что количество транзисторов, которые можно уместить на кремниевом чипе, удваивается примерно каждые два года. Поскольку количество транзисторов связано с вычислительной мощностью, это означало, что, по сути, вычислительная мощность удваивается каждые два года. Так родился <a href="https://ru.wikipedia.org/wiki/Закон_Мура" rel="nofollow">закон Мура</a>, обеспечивший для работающих в компьютерной индустрии людей – по крайней мере, для тех, кому ещё нет сорока – такую же надёжную основу, какую ньютоновские законы движения обеспечели для инженеров-механиков.<br/>
 <a href="https://habr.com/ru/post/484688/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484688#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484688%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484688&amp;event3=%5B%D0%9F%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B4%5D+%D0%9C%D1%8B+%D0%BF%D1%80%D0%B8%D0%B1%D0%BB%D0%B8%D0%B6%D0%B0%D0%B5%D0%BC%D1%81%D1%8F+%D0%BA+%D0%BF%D1%80%D0%B5%D0%B4%D0%B5%D0%BB%D1%83+%D0%B2%D1%8B%D1%87%D0%B8%D1%81%D0%BB%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D1%8B%D1%85+%D0%BC%D0%BE%D1%89%D0%BD%D0%BE%D1%81%D1%82%D0%B5%D0%B9+%E2%80%93+%D0%BD%D0%B0%D0%BC+%D0%BD%D1%83%D0%B6%D0%BD%D1%8B+%D0%BD%D0%BE%D0%B2%D1%8B%D0%B5+%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%81%D1%82%D1%8B&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484688%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484688">https://habr.com/ru/post/484688/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484688</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_515">
													<a href="/testing/?ELEMENT_ID=515"><b>Семинар, конференция, митап: изучаем статистику 18000 мероприятий</b></a><br />
														<small>
			Название:&nbsp;Семинар, конференция, митап: изучаем статистику 18000 мероприятий			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;10:51:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							В Точках кипения проходит до 800 мероприятий в неделю. Одни находят свою аудиторию и вызывают резонанс, другие теряются в информационном шуме.<br>
<br>
Под катом немного статистики, которая поможет как посетителям, так и организаторам получить ответы на вопросы: как мероприятия проходят у других, куда и сколько людей приходит, какие форматы набирают популярность и какой у них индекс планирования, т. е. сколько процентов из зарегистрировавшихся в итоге примут участие.<br>
<br>
<img src="/upload/3bb5b81655693d2fe2b2f17a1e14f89c.png"><br>
<br>
Большая часть цифр получена благодаря анализу статистики Точек кипения с января 2016 года по май 2019 года.<br> <a href="https://habr.com/ru/post/484690/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484690#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484690%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484690&amp;event3=%D0%A1%D0%B5%D0%BC%D0%B8%D0%BD%D0%B0%D1%80%2C+%D0%BA%D0%BE%D0%BD%D1%84%D0%B5%D1%80%D0%B5%D0%BD%D1%86%D0%B8%D1%8F%2C+%D0%BC%D0%B8%D1%82%D0%B0%D0%BF%3A+%D0%B8%D0%B7%D1%83%D1%87%D0%B0%D0%B5%D0%BC+%D1%81%D1%82%D0%B0%D1%82%D0%B8%D1%81%D1%82%D0%B8%D0%BA%D1%83+18000+%D0%BC%D0%B5%D1%80%D0%BE%D0%BF%D1%80%D0%B8%D1%8F%D1%82%D0%B8%D0%B9&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484690%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484690">https://habr.com/ru/post/484690/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484690</a>						</small><br />
			</p>
		<p class="news-item" id="bx_3218110189_514">
													<a href="/testing/?ELEMENT_ID=514"><b>Новинки Dell и Alienware на CES 2020: коротко о главных анонсах</b></a><br />
														<small>
			Название:&nbsp;Новинки Dell и Alienware на CES 2020: коротко о главных анонсах			</small><br />
							<small>
			Дата публикации:&nbsp;
							21.01.2020&nbsp;11:47:00						</small><br />
					<small>
			Краткое описание:&nbsp;
							Чуть больше недели назад завершилась международная выставка Consumer Electronics Show 2020. Для производителей компьютеров и компьютерной техники (то есть и для нас тоже) – это фактически главное мероприятие года. Но так уж вышло, что в России в это время происходит нечто ещё более яркое и интересное – длинные и долгожданные новогодние праздники. В приятной суете многие анонсы остались незамеченными, поэтому мы решили в одной статье собрать всю информацию о ключевых новинках Dell и Alienware на CES 2020.<br>
<br>
<img src="/upload/wfq4ldb7tfu8uezm_i6abqs-tem.jpeg"> <a href="https://habr.com/ru/post/484666/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484666#habracut">Читать дальше &rarr;</a>						</small><br />
					<small>
			Ссылка на статью:&nbsp;
							<a href="/bitrix/redirect.php?event1=news_out&amp;event2=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484666%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484666&amp;event3=%D0%9D%D0%BE%D0%B2%D0%B8%D0%BD%D0%BA%D0%B8+Dell+%D0%B8+Alienware+%D0%BD%D0%B0+CES+2020%3A+%D0%BA%D0%BE%D1%80%D0%BE%D1%82%D0%BA%D0%BE+%D0%BE+%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D1%8B%D1%85+%D0%B0%D0%BD%D0%BE%D0%BD%D1%81%D0%B0%D1%85&amp;goto=https%3A%2F%2Fhabr.com%2Fru%2Fpost%2F484666%2F%3Futm_source%3Dhabrahabr%26utm_medium%3Drss%26utm_campaign%3D484666">https://habr.com/ru/post/484666/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484666</a>						</small><br />
			</p>
	<br />
<font class="text">Новости 


	1 - 20 из 42<br /></font>

	<font class="text">

			Начало&nbsp;|&nbsp;Пред.&nbsp;|
	
	
					<b>1</b>
					
					<a href="/testing/?PAGEN_1=2">2</a>
					
					<a href="/testing/?PAGEN_1=3">3</a>
						|

			<a href="/testing/?PAGEN_1=2">След.</a>&nbsp;|
		<a href="/testing/?PAGEN_1=3">Конец</a>
	



</font></div>
";s:4:"VARS";a:2:{s:8:"arResult";a:7:{s:2:"ID";s:1:"7";s:14:"IBLOCK_TYPE_ID";s:7:"testing";s:13:"LIST_PAGE_URL";s:43:"#SITE_DIR#/testing/index.php?ID=#IBLOCK_ID#";s:15:"NAV_CACHED_DATA";N;s:4:"NAME";s:23:"Статьи хабра";s:7:"SECTION";b:0;s:8:"ELEMENTS";a:20:{i:0;s:3:"533";i:1;s:3:"532";i:2;s:3:"531";i:3;s:3:"530";i:4;s:3:"529";i:5;s:3:"528";i:6;s:3:"527";i:7;s:3:"526";i:8;s:3:"525";i:9;s:3:"524";i:10;s:3:"523";i:11;s:3:"522";i:12;s:3:"521";i:13;s:3:"520";i:14;s:3:"519";i:15;s:3:"518";i:16;s:3:"517";i:17;s:3:"516";i:18;s:3:"515";i:19;s:3:"514";}}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:85:"/bitrix/components/bitrix/news/templates/.default/bitrix/news.list/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;s:17:"__currentCounters";a:1:{s:28:"bitrix:system.pagenavigation";i:1;}s:13:"__editButtons";a:40:{i:0;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"533";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=533&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:1;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"533";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E533&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:2;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"532";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=532&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:3;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"532";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E532&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:4;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"531";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=531&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:5;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"531";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E531&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:6;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"530";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=530&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:7;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"530";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E530&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:8;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"529";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=529&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:9;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"529";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E529&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:10;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"528";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=528&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:11;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"528";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E528&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:12;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"527";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=527&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:13;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"527";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E527&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:14;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"526";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=526&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:15;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"526";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E526&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:16;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"525";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=525&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:17;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"525";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E525&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:18;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"524";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=524&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:19;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"524";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E524&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:20;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"523";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=523&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:21;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"523";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E523&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:22;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"522";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=522&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:23;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"522";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E522&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:24;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"521";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=521&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:25;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"521";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E521&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:26;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"520";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=520&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:27;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"520";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E520&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:28;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"519";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=519&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:29;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"519";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E519&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:30;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"518";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=518&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:31;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"518";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E518&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:32;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"517";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=517&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:33;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"517";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E517&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:34;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"516";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=516&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:35;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"516";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E516&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:36;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"515";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=515&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:37;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"515";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E515&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:38;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"514";i:2;s:209:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=testing&ID=514&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:37:"Изменить публикацию";i:4;a:0:{}}i:39;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"514";i:2;s:160:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=7&type=testing&lang=ru&action=delete&ID=E514&return_url=%2Ftesting%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY";i:3;s:35:"Удалить публикацию";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}}}}}';
return true;
?>