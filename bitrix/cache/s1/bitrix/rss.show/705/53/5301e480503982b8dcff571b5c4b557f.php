<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001579508596';
$dateexpire = '001579512196';
$ser_content = 'a:2:{s:7:"CONTENT";s:17839:"<div class="rss-show">
<h2>Все публикации подряд</h2>
				<p>20.01.2020</p>
				<a href="https://habr.com/ru/post/483318/?utm_source=habrahabr&utm_medium=rss&utm_campaign=483318">Бот для мониторинга веб-сервисов за полчаса: telegram + bash + cron</a>
		<p>
	<img src="https://habrastorage.org/webt/ks/-r/l9/ks-rl9jvsn3vka3r5kxkj4jvbxg.jpeg"><br>
Иногда нужно быстро сделать мониторинг для нового сервиса, а готовой инфраструктуры под рукой нет. В этом гайде мы за полчаса реализуем инструмент для мониторинга любых веб-сервисов, используя только встроенные средства ubuntu: bash, cron и curl. Для доставки оповещений будем использовать telegram.<br>
<br>
«Вишенкой на торте» будет эмоциональное вовлечение пользователей. Проверено на людях — работает.<br> <a href="https://habr.com/ru/post/483318/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=483318#habracut">Читать дальше &rarr;</a>	</p>
	<br />
				<p>20.01.2020</p>
				<a href="https://habr.com/ru/post/483988/?utm_source=habrahabr&utm_medium=rss&utm_campaign=483988">MicroSPA, или как изобрести квадратное колесо</a>
		<p>
	Всем привет, меня зовут Андрей Яковенко, и я веб-разработчик компании Digital Design.<br>
<br>
В нашей компании есть множество проектов, реализованных с помощью системы управления веб-контентом <a href="https://www.progress.com/sitefinity-cms">sitefinity</a>, или по-простому CMS. Причины, по которым мы ее используем, были описаны ранее в <a href="https://habr.com/ru/company/digdes/blog/466419/">этой</a> статье. CMS – это, как правило, Multi Page Application, и сегодня я расскажу о том, что может дать внедрение frontend-фреймворков в решения на sitefinity и как это сделать.<br>
<br>
<img src="https://habrastorage.org/webt/zn/pj/mm/znpjmmafdzi0jcb7qsjvb7rgzoo.png"><br> <a href="https://habr.com/ru/post/483988/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=483988#habracut">Читать дальше &rarr;</a>	</p>
	<br />
				<p>20.01.2020</p>
				<a href="https://habr.com/ru/post/484420/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484420">[Перевод] Изменения в популярном античите BattlEye и способы их обхода</a>
		<p>
	<div style="text-align:center;"><img src="https://habrastorage.org/webt/zd/c7/fe/zdc7ferp6hnuf19n_aid_mh4tca.png"></div><br>
<h2>Основные обновления шелл-кода BattlEye</h2><br>
Время идёт, античиты меняются, и для повышения эффективности продукта в них появляются и исчезают функции. Год назад я подготовил подробное описание шелл-кода BattlEye в своём <a href="https://vmcall.blog/battleye-anticheat-analysis-and-mitigation/" rel="nofollow">блоге</a> [<a href="https://habr.com/ru/post/483068/">перевод</a> на Хабре], и эта часть статьи станет простым отражением изменений, внесённых в шелл-код.<br>
<br>
<h2>Чёрный список временных меток</h2><br>
В последнем анализе BattlEye, в списке теневого бана было всего две метки дат времени компиляции, и похоже, что разработчики решили добавить гораздо больше:<br>
<br>
<code>0x5B12C900 (action_x64.dll)<br>
0x5A180C35 (TerSafe.dll, Epic Games)<br>
0xFC9B9325 (?)<br>
0x456CED13 (d3dx9_32.dll)<br>
0x46495AD9 (d3dx9_34.dll)<br>
0x47CDEE2B (d3dx9_32.dll)<br>
0x469FF22E (d3dx9_35.dll)<br>
0x48EC3AD7 (D3DCompiler_40.dll)<br>
0x5A8E6020 (?)<br>
0x55C85371 (d3dx9_32.dll)<br>
0x456CED13 (?)<br>
0x46495AD9 (D3DCompiler_40.dll)<br>
0x47CDEE2B (D3DX9_37.dll)<br>
0x469FF22E (?)<br>
0x48EC3AD7 (?)<br>
0xFC9B9325 (?)<br>
0x5A8E6020 (?)<br>
0x55C85371 (?)</code><br>
<br>
Мне не удалось идентифицировать оставшиеся временные метки, а два <b>0xF*******</b> — это хеши, созданные детерминированными сборками Visual Studio. Благодарю @mottikraus и T0B1 за идентификацию некоторых временных меток.<br> <a href="https://habr.com/ru/post/484420/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484420#habracut">Читать дальше &rarr;</a>	</p>
	<br />
				<p>20.01.2020</p>
				<a href="https://habr.com/ru/post/484654/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484654">[Перевод] The Ember Times — Выпуск 131</a>
		<p>
	<p><img src="https://habrastorage.org/webt/e2/nn/xf/e2nnxfzrmvngwfzz_xnwouccdb0.png"><br>
<em>От переводчика: Наши скромные усилия по переводу были замечены Ember Learning Team, в этом выпуске они упомянули об этой инициативе русскоговорящего сообщества, что придало переводу небольшой шарм. Но шарм шармом, а это небольшое событие показывает, насколько едино Ember-сообщество и насколько оно ценит усилия отдельных своих представителей.<br>
Как и в прошлый раз ссылки на материалы, на которые я находил перевод, я помечал (рус). Все ссылки без пометки указывают на англоязычные ресурсы. На русском вопросы можно задать в нашем ламповом <a href="https://t.me/ember_js" rel="nofollow">телеграмм-канале</a><br>
</em></p><br>
<p>Привет, Эмберисты! </p><br>
<p>Темы этого выпуска: Помогите улучшить систему автоматического отслеживания (autotracking) и реактивности Ember, читайте RFC (Request For Comments) о новых менеджерах ожиданий в тестах (test waiters), ​​оптимизируйте работу с сервером в своем приложении с помощью Ember Data, вступайте в русское сообщество Ember, и узнайте о мощных техниках отладки приложений на EmberConf !</p> <a href="https://habr.com/ru/post/484654/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484654#habracut">Читать дальше &rarr;</a>	</p>
	<br />
				<p>20.01.2020</p>
				<a href="https://habr.com/ru/post/484628/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484628">Eric Ciaramella как пример цензуры Wikipedia/Google или Трамп vs. Байден</a>
		<p>
	<img src="https://habrastorage.org/webt/ak/qv/kc/akqvkcgteejtjcfsf6p5zrdza74.jpeg" alt="image"><br>
<br>
В статье я расскажу, как меня забанили на английском поддомене Wikipedia по политическим причинам, как огромное количество информационных компаний цензурирует «Того, кого нельзя называть», реально, вбейте на Youtube в комменте «Eric Ciaramella», попробуйте отредактировать, получите 404 ошибку (это прежде чем минусить в карму, коммент удалиться, спорим). А также я расскажу, что я узнал за последний месяц об импичменте Трампа и прифигел. Информационная война-то уже здесь. Надеюсь, что раз я написал про конец Роскомнадзора (да-да, так должна была называться та статья, если бы не цензура НЛО), вы простите мне немного политическую статью, которая тем не менее про цензуру в первую очередь. Также в мою пользу говорит то, что этой информации все еще нет в Российских СМИ.<br> <a href="https://habr.com/ru/post/484628/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484628#habracut">Читать дальше &rarr;</a>	</p>
	<br />
				<p>20.01.2020</p>
				<a href="https://habr.com/ru/post/484648/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484648">Особенности реализации динамических списков в пользовательских интерфейсах</a>
		<p>
	<div style="text-align:center;"><img src="https://habrastorage.org/webt/67/2v/yc/672vyc5rouegebswjkjwctc_z0s.jpeg" alt="image"></div><br>
В интерфейсе каждого современного приложения в том или ином виде присутствуют списки объектов. При работе с ними у пользователя часто возникают потребности в однотипных действиях вроде сортировки, фильтраций, экспорта и так далее. Реализация этих операций часто осложняется тем, что списки могут быть “динамическими”. В этом случае данные будут по мере необходимости считываться не только с сервера на клиент, но и с сервера базы данных на сервер приложений.<br>
<br>
В открытой и бесплатной платформе <a href="https://lsfusion.org">lsFusion</a> все списки по умолчанию являются динамическими и добавляются на любую форму в несколько строк кода. В этой статье я расскажу некоторые технические подробности их реализации, а также возможности в интерфейсе, которые автоматически предоставляются пользователю при работе с любым списком на любой форме.<br> <a href="https://habr.com/ru/post/484648/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484648#habracut">Читать дальше &rarr;</a>	</p>
	<br />
				<p>20.01.2020</p>
				<a href="https://habr.com/ru/post/481932/?utm_source=habrahabr&utm_medium=rss&utm_campaign=481932">[Перевод] Zero Downtime Deployment и базы данных</a>
		<p>
	<img src="https://habrastorage.org/webt/hr/vy/qq/hrvyqqi2mmitehw9vx_xklbofj8.png"><br>
<p>В этой статье подробно объясняется, как решать проблемы, связанные с совместимостью баз данных при деплое. Мы расскажем, что может произойти с вашими приложениями на проде, если вы попытаетесь выполнить деплой без предварительной подготовки. Затем мы пройдемся по этапам жизненного цикла приложения, которые необходимы, чтобы иметь нулевое время простоя (<em>прим. пер.: далее — zero downtime</em>). Результатом наших операций будет применение обратно несовместимого изменения базы данных обратно совместимым способом.</p> <a href="https://habr.com/ru/post/481932/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=481932#habracut">Читать дальше &rarr;</a>	</p>
	<br />
				<p>20.01.2020</p>
				<a href="https://habr.com/ru/post/484646/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484646">Как я искала IT-Волшебника</a>
		<p>
	Когда занимаешься подбором IT-специалистов для различных компаний самое сложное выяснить кто нужен заказчику, с какими скиллами и чем, собственно, он будет заниматься. <br>
Неделю назад ко мне попал заказ — крупная компания в поиске Функционального архитектора. Чтобы вы понимали, в вакансии только общие фразы и очень мало конкретики. На стороне заказчика начинающий HR и доступ к лицу, принимающему решения закрыт. Ответы на уточняющие вопросы мне не сильно помогли, но какое-то понимание появилось, что нужен специалист с широкой эрудицией, знанием множества систем и практическим опытом интеграций между ними. При этом требуются не только технические скиллы разработчика/devOps-а, но и умение выяснять потребности бизнеса и отстаивать свои предложения перед топ-менеджментом.<br>
<br>
<img src="https://habrastorage.org/webt/fu/mw/kj/fumwkjnb7rxex9zlx8focgnm66e.jpeg" alt="image"><br>
<br>
В начале, мне показалось, что заказчик ищет мифическое существо, которого в реальной жизни не существует. Но потом я подумала – а, что если отбросить привычный шаблон вакансии и написать так как я ее чувствовала в тот момент.<br> <a href="https://habr.com/ru/post/484646/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484646#habracut">Читать дальше &rarr;</a>	</p>
	<br />
				<p>20.01.2020</p>
				<a href="https://habr.com/ru/post/484642/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484642">Карма — это приглашение на вечеринку</a>
		<p>
	Рискуя кармой (хе-хе), хочу принять участие в дискуссии и ответить автору <a href="https://habr.com/ru/post/484566/">этого поста</a>. В принципе, можно было бы ограничиться заголовком, но поскольку у нас тут Хабр, а не Твиттер, я растекусь мысию по древу, серым волком по земле, сизым орлом под облаками.<br>
<br>
<img src="https://habrastorage.org/getpro/habr/post_images/49a/7f0/12d/49a7f012d2ce06ef344814d0da91f863.jpg" alt="image"><br> <a href="https://habr.com/ru/post/484642/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484642#habracut">Читать дальше &rarr;</a>	</p>
	<br />
				<p>20.01.2020</p>
				<a href="https://habr.com/ru/post/484640/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484640">Nginx-log-collector утилита от Авито для отправки логов nginx в Clickhouse</a>
		<p>
	<p>В этой статье будет рассматриваться проект <a href="https://github.com/avito-tech/nginx-log-collector" rel="nofollow">nginx-log-collector</a>, который будет читать логи nginx, отправлять их в кластер Clickhouse. Обычно для логов используют ElasticSearch. Для Clickhouse требуется меньше ресурсов (дисковое пространство, ОЗУ, ЦПУ). Clickhouse быстрее записывает данные. Clickhouse сжимает данные, что делает данные на диске еще компактнее. Преимущества Clickhouse видны по 2 слайдам с доклада <a href="https://habr.com/ru/company/ua-hosting/blog/483712/">Как VK вставляет данные в ClickHouse с десятков тысяч серверов.</a></p><br>
<p><img src="https://habrastorage.org/webt/2s/xr/fw/2sxrfwyzgj72wfqblvhlu8fphf0.jpeg"></p><br>
<p><img src="https://habrastorage.org/webt/wb/uv/ik/wbuvikg6tm42r0pjhkrtdrcye08.jpeg"></p><br>
<p>Для просмотра аналитики по логам создадим дашборд для Grafana.</p><br>
<p>Кому интересно, добро пожаловать под кат.</p> <a href="https://habr.com/ru/post/484640/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484640#habracut">Читать дальше &rarr;</a>	</p>
	<br />
</div>";s:4:"VARS";a:2:{s:8:"arResult";a:7:{s:5:"title";s:40:"Все публикации подряд";s:4:"link";s:28:"https://habr.com/ru/all/all/";s:11:"description";s:56:"Все публикации подряд на Хабре";s:13:"lastBuildDate";s:0:"";s:3:"ttl";N;s:5:"image";a:5:{s:5:"title";s:8:"Хабр";s:3:"url";s:32:"https://habr.com/images/logo.png";s:4:"link";s:20:"https://habr.com/ru/";s:5:"width";N;s:6:"height";N;}s:4:"item";a:10:{i:0;a:5:{s:11:"description";s:962:"<img src="https://habrastorage.org/webt/ks/-r/l9/ks-rl9jvsn3vka3r5kxkj4jvbxg.jpeg"><br>
Иногда нужно быстро сделать мониторинг для нового сервиса, а готовой инфраструктуры под рукой нет. В этом гайде мы за полчаса реализуем инструмент для мониторинга любых веб-сервисов, используя только встроенные средства ubuntu: bash, cron и curl. Для доставки оповещений будем использовать telegram.<br>
<br>
«Вишенкой на торте» будет эмоциональное вовлечение пользователей. Проверено на людях — работает.<br> <a href="https://habr.com/ru/post/483318/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=483318#habracut">Читать дальше &rarr;</a>";s:5:"title";s:104:"Бот для мониторинга веб-сервисов за полчаса: telegram + bash + cron";s:4:"link";s:88:"https://habr.com/ru/post/483318/?utm_source=habrahabr&utm_medium=rss&utm_campaign=483318";s:8:"category";s:50:"Системы обмена сообщениями";s:7:"pubDate";s:29:"Mon, 20 Jan 2020 08:15:38 GMT";}i:1;a:5:{s:11:"description";s:1136:"Всем привет, меня зовут Андрей Яковенко, и я веб-разработчик компании Digital Design.<br>
<br>
В нашей компании есть множество проектов, реализованных с помощью системы управления веб-контентом <a href="https://www.progress.com/sitefinity-cms">sitefinity</a>, или по-простому CMS. Причины, по которым мы ее используем, были описаны ранее в <a href="https://habr.com/ru/company/digdes/blog/466419/">этой</a> статье. CMS – это, как правило, Multi Page Application, и сегодня я расскажу о том, что может дать внедрение frontend-фреймворков в решения на sitefinity и как это сделать.<br>
<br>
<img src="https://habrastorage.org/webt/zn/pj/mm/znpjmmafdzi0jcb7qsjvb7rgzoo.png"><br> <a href="https://habr.com/ru/post/483988/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=483988#habracut">Читать дальше &rarr;</a>";s:5:"title";s:76:"MicroSPA, или как изобрести квадратное колесо";s:4:"link";s:88:"https://habr.com/ru/post/483988/?utm_source=habrahabr&utm_medium=rss&utm_campaign=483988";s:8:"category";s:40:"Блог компании Digital Design";s:7:"pubDate";s:29:"Mon, 20 Jan 2020 08:05:14 GMT";}i:2;a:5:{s:11:"description";s:2258:"<div style="text-align:center;"><img src="https://habrastorage.org/webt/zd/c7/fe/zdc7ferp6hnuf19n_aid_mh4tca.png"></div><br>
<h2>Основные обновления шелл-кода BattlEye</h2><br>
Время идёт, античиты меняются, и для повышения эффективности продукта в них появляются и исчезают функции. Год назад я подготовил подробное описание шелл-кода BattlEye в своём <a href="https://vmcall.blog/battleye-anticheat-analysis-and-mitigation/" rel="nofollow">блоге</a> [<a href="https://habr.com/ru/post/483068/">перевод</a> на Хабре], и эта часть статьи станет простым отражением изменений, внесённых в шелл-код.<br>
<br>
<h2>Чёрный список временных меток</h2><br>
В последнем анализе BattlEye, в списке теневого бана было всего две метки дат времени компиляции, и похоже, что разработчики решили добавить гораздо больше:<br>
<br>
<code>0x5B12C900 (action_x64.dll)<br>
0x5A180C35 (TerSafe.dll, Epic Games)<br>
0xFC9B9325 (?)<br>
0x456CED13 (d3dx9_32.dll)<br>
0x46495AD9 (d3dx9_34.dll)<br>
0x47CDEE2B (d3dx9_32.dll)<br>
0x469FF22E (d3dx9_35.dll)<br>
0x48EC3AD7 (D3DCompiler_40.dll)<br>
0x5A8E6020 (?)<br>
0x55C85371 (d3dx9_32.dll)<br>
0x456CED13 (?)<br>
0x46495AD9 (D3DCompiler_40.dll)<br>
0x47CDEE2B (D3DX9_37.dll)<br>
0x469FF22E (?)<br>
0x48EC3AD7 (?)<br>
0xFC9B9325 (?)<br>
0x5A8E6020 (?)<br>
0x55C85371 (?)</code><br>
<br>
Мне не удалось идентифицировать оставшиеся временные метки, а два <b>0xF*******</b> — это хеши, созданные детерминированными сборками Visual Studio. Благодарю @mottikraus и T0B1 за идентификацию некоторых временных меток.<br> <a href="https://habr.com/ru/post/484420/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484420#habracut">Читать дальше &rarr;</a>";s:5:"title";s:121:"[Перевод] Изменения в популярном античите BattlEye и способы их обхода";s:4:"link";s:88:"https://habr.com/ru/post/484420/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484420";s:8:"category";s:53:"Информационная безопасность";s:7:"pubDate";s:29:"Mon, 20 Jan 2020 07:55:46 GMT";}i:3;a:5:{s:11:"description";s:1990:"<p><img src="https://habrastorage.org/webt/e2/nn/xf/e2nnxfzrmvngwfzz_xnwouccdb0.png"><br>
<em>От переводчика: Наши скромные усилия по переводу были замечены Ember Learning Team, в этом выпуске они упомянули об этой инициативе русскоговорящего сообщества, что придало переводу небольшой шарм. Но шарм шармом, а это небольшое событие показывает, насколько едино Ember-сообщество и насколько оно ценит усилия отдельных своих представителей.<br>
Как и в прошлый раз ссылки на материалы, на которые я находил перевод, я помечал (рус). Все ссылки без пометки указывают на англоязычные ресурсы. На русском вопросы можно задать в нашем ламповом <a href="https://t.me/ember_js" rel="nofollow">телеграмм-канале</a><br>
</em></p><br>
<p>Привет, Эмберисты! </p><br>
<p>Темы этого выпуска: Помогите улучшить систему автоматического отслеживания (autotracking) и реактивности Ember, читайте RFC (Request For Comments) о новых менеджерах ожиданий в тестах (test waiters), ​​оптимизируйте работу с сервером в своем приложении с помощью Ember Data, вступайте в русское сообщество Ember, и узнайте о мощных техниках отладки приложений на EmberConf !</p> <a href="https://habr.com/ru/post/484654/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484654#habracut">Читать дальше &rarr;</a>";s:5:"title";s:53:"[Перевод] The Ember Times — Выпуск 131";s:4:"link";s:88:"https://habr.com/ru/post/484654/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484654";s:8:"category";s:40:"Разработка веб-сайтов";s:7:"pubDate";s:29:"Mon, 20 Jan 2020 07:38:27 GMT";}i:4;a:5:{s:11:"description";s:1636:"<img src="https://habrastorage.org/webt/ak/qv/kc/akqvkcgteejtjcfsf6p5zrdza74.jpeg" alt="image"><br>
<br>
В статье я расскажу, как меня забанили на английском поддомене Wikipedia по политическим причинам, как огромное количество информационных компаний цензурирует «Того, кого нельзя называть», реально, вбейте на Youtube в комменте «Eric Ciaramella», попробуйте отредактировать, получите 404 ошибку (это прежде чем минусить в карму, коммент удалиться, спорим). А также я расскажу, что я узнал за последний месяц об импичменте Трампа и прифигел. Информационная война-то уже здесь. Надеюсь, что раз я написал про конец Роскомнадзора (да-да, так должна была называться та статья, если бы не цензура НЛО), вы простите мне немного политическую статью, которая тем не менее про цензуру в первую очередь. Также в мою пользу говорит то, что этой информации все еще нет в Российских СМИ.<br> <a href="https://habr.com/ru/post/484628/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484628#habracut">Читать дальше &rarr;</a>";s:5:"title";s:102:"Eric Ciaramella как пример цензуры Wikipedia/Google или Трамп vs. Байден";s:4:"link";s:88:"https://habr.com/ru/post/484628/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484628";s:8:"category";s:17:"Я пиарюсь";s:7:"pubDate";s:29:"Mon, 20 Jan 2020 07:37:04 GMT";}i:5;a:5:{s:11:"description";s:1816:"<div style="text-align:center;"><img src="https://habrastorage.org/webt/67/2v/yc/672vyc5rouegebswjkjwctc_z0s.jpeg" alt="image"></div><br>
В интерфейсе каждого современного приложения в том или ином виде присутствуют списки объектов. При работе с ними у пользователя часто возникают потребности в однотипных действиях вроде сортировки, фильтраций, экспорта и так далее. Реализация этих операций часто осложняется тем, что списки могут быть “динамическими”. В этом случае данные будут по мере необходимости считываться не только с сервера на клиент, но и с сервера базы данных на сервер приложений.<br>
<br>
В открытой и бесплатной платформе <a href="https://lsfusion.org">lsFusion</a> все списки по умолчанию являются динамическими и добавляются на любую форму в несколько строк кода. В этой статье я расскажу некоторые технические подробности их реализации, а также возможности в интерфейсе, которые автоматически предоставляются пользователю при работе с любым списком на любой форме.<br> <a href="https://habr.com/ru/post/484648/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484648#habracut">Читать дальше &rarr;</a>";s:5:"title";s:142:"Особенности реализации динамических списков в пользовательских интерфейсах";s:4:"link";s:88:"https://habr.com/ru/post/484648/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484648";s:8:"category";s:34:"Блог компании lsFusion";s:7:"pubDate";s:29:"Mon, 20 Jan 2020 07:12:26 GMT";}i:6;a:5:{s:11:"description";s:1183:"<img src="https://habrastorage.org/webt/hr/vy/qq/hrvyqqi2mmitehw9vx_xklbofj8.png"><br>
<p>В этой статье подробно объясняется, как решать проблемы, связанные с совместимостью баз данных при деплое. Мы расскажем, что может произойти с вашими приложениями на проде, если вы попытаетесь выполнить деплой без предварительной подготовки. Затем мы пройдемся по этапам жизненного цикла приложения, которые необходимы, чтобы иметь нулевое время простоя (<em>прим. пер.: далее — zero downtime</em>). Результатом наших операций будет применение обратно несовместимого изменения базы данных обратно совместимым способом.</p> <a href="https://habr.com/ru/post/481932/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=481932#habracut">Читать дальше &rarr;</a>";s:5:"title";s:66:"[Перевод] Zero Downtime Deployment и базы данных";s:4:"link";s:88:"https://habr.com/ru/post/481932/?utm_source=habrahabr&utm_medium=rss&utm_campaign=481932";s:8:"category";s:31:"Блог компании Nixys";s:7:"pubDate";s:29:"Mon, 20 Jan 2020 07:09:32 GMT";}i:7;a:5:{s:11:"description";s:2105:"Когда занимаешься подбором IT-специалистов для различных компаний самое сложное выяснить кто нужен заказчику, с какими скиллами и чем, собственно, он будет заниматься. <br>
Неделю назад ко мне попал заказ — крупная компания в поиске Функционального архитектора. Чтобы вы понимали, в вакансии только общие фразы и очень мало конкретики. На стороне заказчика начинающий HR и доступ к лицу, принимающему решения закрыт. Ответы на уточняющие вопросы мне не сильно помогли, но какое-то понимание появилось, что нужен специалист с широкой эрудицией, знанием множества систем и практическим опытом интеграций между ними. При этом требуются не только технические скиллы разработчика/devOps-а, но и умение выяснять потребности бизнеса и отстаивать свои предложения перед топ-менеджментом.<br>
<br>
<img src="https://habrastorage.org/webt/fu/mw/kj/fumwkjnb7rxex9zlx8focgnm66e.jpeg" alt="image"><br>
<br>
В начале, мне показалось, что заказчик ищет мифическое существо, которого в реальной жизни не существует. Но потом я подумала – а, что если отбросить привычный шаблон вакансии и написать так как я ее чувствовала в тот момент.<br> <a href="https://habr.com/ru/post/484646/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484646#habracut">Читать дальше &rarr;</a>";s:5:"title";s:46:"Как я искала IT-Волшебника";s:4:"link";s:88:"https://habr.com/ru/post/484646/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484646";s:8:"category";s:41:"Управление персоналом";s:7:"pubDate";s:29:"Mon, 20 Jan 2020 07:05:15 GMT";}i:8;a:5:{s:11:"description";s:798:"Рискуя кармой (хе-хе), хочу принять участие в дискуссии и ответить автору <a href="https://habr.com/ru/post/484566/">этого поста</a>. В принципе, можно было бы ограничиться заголовком, но поскольку у нас тут Хабр, а не Твиттер, я растекусь мысию по древу, серым волком по земле, сизым орлом под облаками.<br>
<br>
<img src="https://habrastorage.org/getpro/habr/post_images/49a/7f0/12d/49a7f012d2ce06ef344814d0da91f863.jpg" alt="image"><br> <a href="https://habr.com/ru/post/484642/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484642#habracut">Читать дальше &rarr;</a>";s:5:"title";s:68:"Карма — это приглашение на вечеринку";s:4:"link";s:88:"https://habr.com/ru/post/484642/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484642";s:8:"category";s:4:"Habr";s:7:"pubDate";s:29:"Mon, 20 Jan 2020 07:04:09 GMT";}i:9;a:5:{s:11:"description";s:1483:"<p>В этой статье будет рассматриваться проект <a href="https://github.com/avito-tech/nginx-log-collector" rel="nofollow">nginx-log-collector</a>, который будет читать логи nginx, отправлять их в кластер Clickhouse. Обычно для логов используют ElasticSearch. Для Clickhouse требуется меньше ресурсов (дисковое пространство, ОЗУ, ЦПУ). Clickhouse быстрее записывает данные. Clickhouse сжимает данные, что делает данные на диске еще компактнее. Преимущества Clickhouse видны по 2 слайдам с доклада <a href="https://habr.com/ru/company/ua-hosting/blog/483712/">Как VK вставляет данные в ClickHouse с десятков тысяч серверов.</a></p><br>
<p><img src="https://habrastorage.org/webt/2s/xr/fw/2sxrfwyzgj72wfqblvhlu8fphf0.jpeg"></p><br>
<p><img src="https://habrastorage.org/webt/wb/uv/ik/wbuvikg6tm42r0pjhkrtdrcye08.jpeg"></p><br>
<p>Для просмотра аналитики по логам создадим дашборд для Grafana.</p><br>
<p>Кому интересно, добро пожаловать под кат.</p> <a href="https://habr.com/ru/post/484640/?utm_source=habrahabr&amp;utm_medium=rss&amp;utm_campaign=484640#habracut">Читать дальше &rarr;</a>";s:5:"title";s:105:"Nginx-log-collector утилита от Авито для отправки логов nginx в Clickhouse";s:4:"link";s:88:"https://habr.com/ru/post/484640/?utm_source=habrahabr&utm_medium=rss&utm_campaign=484640";s:8:"category";s:53:"Системное администрирование";s:7:"pubDate";s:29:"Mon, 20 Jan 2020 07:04:05 GMT";}}}s:18:"templateCachedData";a:1:{s:9:"frameMode";b:1;}}}';
return true;
?>