<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("testing");
?>

<?
function getImgFullName($value)
{
    return $_SERVER['DOCUMENT_ROOT'] . '/upload/' . $value;
}

function getImgPath($value)
{
    return SITE_DIR . 'upload/' . $value;
}

function getRenamedImages($value, array $matches)
{
    $path = array();
    foreach ($matches[2] as $match) {
        $path[] = getImgPath($match);
    }
    return str_replace($matches[1], $path, $value);
}

if (CModule::IncludeModule("iblock")) {
    $xmlLink = file_get_contents('https://habr.com/ru/rss/all/all/');
    $xml = new SimpleXMLElement($xmlLink);
    $el = new CIBlockElement;
    $iBlockElementTitles = array();
    $xmlElementItems = array();
    $newArticlesCount = 0;

    $arSelect = array("ID", "NAME");
    $arFilter = array(
        "IBLOCK_ID" => 7,
    );
    $rsItems = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
    while ($ob = $rsItems->GetNext()) {
        $iBlockElementTitles[$ob["NAME"]] = $ob["ID"];
    }

    foreach ($xml->channel->item as $item) {

        $description = $item->description->__toString();
        $regexp = '/src\=\"(\S+\/(\S+\.(jpeg|jpg|png|gif)))\"/';
        $matches = array();

        if (preg_match_all($regexp, $description, $matches)) {

            $description = getRenamedImages($description, $matches);

            foreach ($matches[2] as $key => $match) {
                $images[] = [
                    'FULL_NAME' => getImgFullName($match),
                    'URL'       => $matches[1][$key],
                ];
            }
        }

        $xmlElementItems[$item->title->__toString()] = [
            'QUICK_TEXT' => $description,
            'PUB_LINK'   => $item->link->__toString(),
            'PUB_DATE'   => date("d.m.Y H:i", strtotime($item->pubDate)),
            'IMAGES'     => $images,
        ];
        
        $arrayDiff = array_diff_key($xmlElementItems, $iBlockElementTitles);
    }

    if (is_array($arrayDiff)) {
        foreach ($arrayDiff as $name => $values) {

            $arLoadProductArray = Array(
                "MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
                "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
                "IBLOCK_ID" => 7,
                "PROPERTY_VALUES" => $values,
                "NAME" => $name,
                "ACTIVE" => "Y",            // активен
            );

            if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                foreach ($values['IMAGES'] as $image) {
                    file_put_contents($image['FULL_NAME'], file_get_contents($image['URL']));
                }
                $newArticlesCount++;
            } else {
                echo "Error: " . $el->LAST_ERROR;
            }
        }
    }
}

echo '<i style="text-decoration: underline;">Добавлено статей:' . $newArticlesCount . '</i><hr>';
?>
<? $APPLICATION->IncludeComponent(
    "bitrix:news",
    "",
    Array(
        "ADD_ELEMENT_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "Y",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "BROWSER_TITLE" => "-",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.y G:i",
        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
        "DETAIL_DISPLAY_TOP_PAGER" => "N",
        "DETAIL_FIELD_CODE" => array("NAME", ""),
        "DETAIL_PAGER_SHOW_ALL" => "Y",
        "DETAIL_PAGER_TEMPLATE" => "",
        "DETAIL_PAGER_TITLE" => "Страница",
        "DETAIL_PROPERTY_CODE" => array("PUB_DATE", "QUICK_TEXT", "PUB_LINK", ""),
        "DETAIL_SET_CANONICAL_URL" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "7",
        "IBLOCK_TYPE" => "testing",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "LIST_ACTIVE_DATE_FORMAT" => "d.m.y G:i",
        "LIST_FIELD_CODE" => array("NAME", ""),
        "LIST_PROPERTY_CODE" => array("PUB_DATE", "QUICK_TEXT", "PUB_LINK", ""),
        "MESSAGE_404" => "",
        "META_DESCRIPTION" => "-",
        "META_KEYWORDS" => "-",
        "NEWS_COUNT" => "20",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PREVIEW_TRUNCATE_LEN" => "",
        "SEF_MODE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N",
        "USE_CATEGORIES" => "N",
        "USE_FILTER" => "N",
        "USE_PERMISSIONS" => "N",
        "USE_RATING" => "N",
        "USE_REVIEW" => "N",
        "USE_RSS" => "N",
        "USE_SEARCH" => "N",
        "USE_SHARE" => "N",
        "VARIABLE_ALIASES" => Array("ELEMENT_ID" => "ELEMENT_ID", "SECTION_ID" => "SECTION_ID")
    )
); ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>

